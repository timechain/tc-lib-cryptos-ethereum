import { BigNumber } from 'bignumber.js';
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models';
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet';
import { EthereumWallet } from './models/wallet';
import { EthersProvider } from './vendor/ethers';
export interface SendEthereumTransactionOptions {
    nonce?: number;
}
export declare class EthereumAdapterClass implements WalletAdapter<EthereumWallet> {
    crypto: string;
    name: string;
    decimals: number;
    provider: EthersProvider;
    generateRandomWallet(): Promise<{
        wallet: EthereumWallet;
        seed: string;
    }>;
    importWalletWithSeed(seed: string): Promise<EthereumWallet>;
    importWallet(info: {
        publicKey?: string;
        privateKey?: string;
        address?: string;
    }): Promise<EthereumWallet>;
    getBalance(wallet: Partial<Wallet>): Promise<BigNumber>;
    sendTransaction(options: SendTransactionOptions<EthereumWallet> & SendEthereumTransactionOptions): Promise<Transaction>;
    getTransactionFees(params: SendTransactionOptions<EthereumWallet>): Promise<BigNumber>;
    getTransactions(wallet: Partial<Wallet>): Promise<Transaction[]>;
    validateAddress(address: string, options: object | null): boolean;
    getNonce(address: string): Promise<number>;
}
export declare const EthereumAdapter: EthereumAdapterClass;
