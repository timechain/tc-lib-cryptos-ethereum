"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Web3 = require("web3");
var ethers = require("ethers");
var constants_1 = require("./utils/constants");
var etherscan_1 = require("./vendor/etherscan");
var ethers_1 = require("./vendor/ethers");
var EthereumAdapterClass = /** @class */ (function () {
    function EthereumAdapterClass() {
        this.crypto = constants_1.CRYPTO;
        this.name = 'Ethereum';
        this.decimals = 18;
        this.provider = new ethers.providers.getDefaultProvider();
    }
    // private provider: EthersProvider = new ethers.providers.JsonRpcProvider()
    EthereumAdapterClass.prototype.generateRandomWallet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var wallet;
            return __generator(this, function (_a) {
                wallet = new ethers.Wallet.createRandom();
                return [2 /*return*/, {
                        seed: wallet.mnemonic,
                        wallet: ethers_1.ethersWalletToEthereumWallet(wallet)
                    }];
            });
        });
    };
    EthereumAdapterClass.prototype.importWalletWithSeed = function (seed) {
        return __awaiter(this, void 0, void 0, function () {
            var wallet;
            return __generator(this, function (_a) {
                wallet = ethers.Wallet.fromMnemonic(seed);
                return [2 /*return*/, ethers_1.ethersWalletToEthereumWallet(wallet)];
            });
        });
    };
    EthereumAdapterClass.prototype.importWallet = function (info) {
        return __awaiter(this, void 0, void 0, function () {
            var wallet;
            return __generator(this, function (_a) {
                if (!info.privateKey)
                    throw new Error('Private key not provided.');
                wallet = new ethers.Wallet(info.privateKey);
                return [2 /*return*/, ethers_1.ethersWalletToEthereumWallet(wallet)];
            });
        });
    };
    EthereumAdapterClass.prototype.getBalance = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            var balance;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.provider.getBalance(wallet.address)];
                    case 1:
                        balance = _a.sent();
                        return [2 /*return*/, ethers_1.ethersBigNumberToBigNumber(balance)];
                }
            });
        });
    };
    EthereumAdapterClass.prototype.sendTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var ethWallet, sendOptions, _a, transaction;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        ethWallet = ethers_1.ethersWallet(options.wallet.privateKey, this.provider);
                        _a = {};
                        return [4 /*yield*/, ethWallet.estimateGas({
                                to: options.recipientAddress,
                                value: ethers_1.toEthersAmount(options.amount)
                            })];
                    case 1:
                        _a.gasLimit = _b.sent();
                        return [4 /*yield*/, this.provider.getGasPrice()];
                    case 2:
                        sendOptions = (_a.gasPrice = _b.sent(),
                            _a);
                        return [4 /*yield*/, ethWallet.send(options.recipientAddress, ethers_1.toEthersAmount(options.amount), sendOptions)];
                    case 3:
                        transaction = _b.sent();
                        return [2 /*return*/, {
                                hash: transaction.hash,
                                date: new Date(),
                                fromAddress: transaction.from,
                                toAddress: transaction.to,
                                amount: options.amount,
                                crypto: this.crypto
                            }];
                }
            });
        });
    };
    EthereumAdapterClass.prototype.getTransactionFees = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var ethWallet, gasUsed, gasPrice, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        ethWallet = ethers_1.ethersWallet(params.wallet.privateKey, this.provider);
                        return [4 /*yield*/, ethWallet.estimateGas({
                                to: params.recipientAddress,
                                value: ethers_1.toEthersAmount(params.amount)
                            })];
                    case 1:
                        gasUsed = _b.sent();
                        _a = ethers_1.ethersBigNumberToBigNumber;
                        return [4 /*yield*/, this.provider.getGasPrice()];
                    case 2:
                        gasPrice = _a.apply(void 0, [_b.sent()]);
                        return [2 /*return*/, gasPrice.multipliedBy(gasUsed)];
                }
            });
        });
    };
    EthereumAdapterClass.prototype.getTransactions = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            var transactions, etherscanTransactions, _i, etherscanTransactions_1, transaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!wallet.address)
                            throw new Error('The provided wallet must have an address');
                        transactions = [];
                        return [4 /*yield*/, etherscan_1.getTransactions(wallet.address)];
                    case 1:
                        etherscanTransactions = _a.sent();
                        for (_i = 0, etherscanTransactions_1 = etherscanTransactions; _i < etherscanTransactions_1.length; _i++) {
                            transaction = etherscanTransactions_1[_i];
                            transactions.push(etherscan_1.toTransaction(transaction));
                        }
                        return [2 /*return*/, transactions];
                }
            });
        });
    };
    EthereumAdapterClass.prototype.validateAddress = function (address, options) {
        var web3 = new Web3(this.provider);
        return web3._extend.utils.isAddress(address);
    };
    EthereumAdapterClass.prototype.getNonce = function (address) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, etherscan_1.getNonce(address)];
            });
        });
    };
    return EthereumAdapterClass;
}());
exports.EthereumAdapterClass = EthereumAdapterClass;
exports.EthereumAdapter = new EthereumAdapterClass();
