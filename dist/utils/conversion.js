"use strict";
exports.__esModule = true;
function gweiToWei(gwei) {
    return gwei.multipliedBy(1000000000);
}
exports.gweiToWei = gweiToWei;
