import { BigNumber } from 'bignumber.js';
import { TransactionFeesPolicy } from 'tc-lib-cryptos/dist/modules/wallet';
export declare const GAS_API = "https://ethgasstation.info/json/ethgasAPI.json";
export declare function getGasPrice(policy: TransactionFeesPolicy): Promise<BigNumber>;
