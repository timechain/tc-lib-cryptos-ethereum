import { Transaction } from 'tc-lib-cryptos/dist/models';
export interface EtherscanTransaction {
    blockNumber: string;
    timeStamp: string;
    hash: string;
    nonce: string;
    blockHash: string;
    from: string;
    contractAddress: string;
    to: string;
    value: string;
    tokenName: string;
    tokenSymbol: string;
    tokenDecimal: string;
    transactionIndex: string;
    gas: string;
    gasPrice: string;
    gasUsed: string;
    cumulativeGasUsed: string;
    input: string;
    confirmations: string;
}
export declare function getTransactions(address: string): Promise<EtherscanTransaction[]>;
export declare function toTransaction(etherscanTransaction: EtherscanTransaction, crypto?: string): Transaction;
export declare function getNonce(address: string): Promise<number>;
