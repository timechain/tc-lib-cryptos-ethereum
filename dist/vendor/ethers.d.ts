import * as ethers from 'ethers';
import { BigNumber } from 'bignumber.js';
import { EthereumWallet } from '../models/wallet';
export declare type BlockTag = number | string | 'latest' | 'pending';
export interface EthersBigNumber {
    add(otherValue: EthersBigNumber): EthersBigNumber;
    sub(otherValue: EthersBigNumber): EthersBigNumber;
    mul(otherValue: EthersBigNumber): EthersBigNumber;
    div(otherValue: EthersBigNumber): EthersBigNumber;
    mod(otherValue: EthersBigNumber): EthersBigNumber;
    eq(otherValue: EthersBigNumber): EthersBigNumber;
    lt(otherValue: EthersBigNumber): EthersBigNumber;
    lte(otherValue: EthersBigNumber): EthersBigNumber;
    gt(otherValue: EthersBigNumber): EthersBigNumber;
    gte(otherValue: EthersBigNumber): EthersBigNumber;
    isZero(): boolean;
    toNumber(): number;
    toString(): string;
    toHexString(): string;
}
export interface EthersTransaction {
    blockHash: string;
    blockNumber: number;
    transactionIndex: number;
    to: string;
    hash: string;
    data: string;
    from: string;
    gasLimit: EthersBigNumber;
    gasPrice: EthersBigNumber;
    nonce: number;
    value: EthersBigNumber;
    networkId: number;
    r: string;
    s: string;
    v: number;
    raw: string;
}
export interface EthersTransactionReceipt {
    transactionHash: string;
    blockHash: string;
    blockNumber: number;
    transactionIndex: number;
    contractAddress: string | null;
    cumulativeGasUsed: EthersBigNumber;
    gasUsed: EthersBigNumber;
    log: any[];
    logBloom: any;
    byzantium: boolean;
    root: string;
    status: number;
}
export interface EthersWallet {
    /**
     * Returns a Promise with the balance of the wallet (as a BigNumber, in wei) at the blockTag.
     */
    getBalance(blockTag?: BlockTag): Promise<EthersBigNumber>;
    /**
     * Returns a Promise with the estimated cost for transaction (in gas, as a BigNumber)
     */
    estimateGas(transaction: EthersTransaction): Promise<EthersBigNumber>;
    /**
     * Sends amountWei to addressOrName on the network and returns a Promise with the transaction details.
     */
    send(addressOrName: string, amountWei: EthersBigNumber, options: any): Promise<EthersTransaction>;
}
export interface EthersProvider {
    /** The name of the network the provider is connected to (e.g. ‘homestead’, ‘ropsten’, ‘rinkeby’, ‘kovan’) */
    name: string;
    /** The chain ID (or network ID) this provider is connected as; this is used by signers to prevent replay attacks across compatible networks */
    chainId: string;
    /** Returns a Promise with the balance (as a BigNumber) of addressOrName at blockTag. (See: Block Tags) */
    getBalance(addressOrName: string, blockTag?: BlockTag): Promise<EthersBigNumber>;
    /** Returns a Promise with the latest block number (as a Number). */
    getBlockNumber(): Promise<number>;
    /** Returns a Promise with the current gas price (as a BigNumber). */
    getGasPrice(): Promise<EthersBigNumber>;
    /** Returns a Promise with the transaction with transactionHash. (See: Transaction Responses) */
    getTransaction(hash: string): Promise<EthersTransaction>;
    /** Returns a Promise with the transaction receipt with transactionHash. (See: Transaction Receipts) */
    getTransactionReceipt(hash: string): Promise<EthersTransactionReceipt>;
}
export declare function ethersWallet(privateKey: string, provider: any): any;
export declare function ethersBigNumberToBigNumber(value: EthersBigNumber): BigNumber;
export declare function toEthersAmount(amount: BigNumber): any;
export declare function ethersWalletToEthereumWallet(wallet: ethers.Wallet): EthereumWallet;
