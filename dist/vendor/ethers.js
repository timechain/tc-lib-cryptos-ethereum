"use strict";
exports.__esModule = true;
var ethers = require("ethers");
var bignumber_js_1 = require("bignumber.js");
var constants_1 = require("../utils/constants");
function ethersWallet(privateKey, provider) {
    return new ethers.Wallet(privateKey, provider);
}
exports.ethersWallet = ethersWallet;
function ethersBigNumberToBigNumber(value) {
    return new bignumber_js_1.BigNumber(value);
}
exports.ethersBigNumberToBigNumber = ethersBigNumberToBigNumber;
function toEthersAmount(amount) {
    return ethers.utils.bigNumberify(amount.toString(10));
}
exports.toEthersAmount = toEthersAmount;
function ethersWalletToEthereumWallet(wallet) {
    var publicKey = new ethers.SigningKey(wallet.privateKey).publicKey;
    return {
        crypto: constants_1.CRYPTO,
        address: wallet.address,
        publicKey: publicKey,
        privateKey: wallet.privateKey
    };
}
exports.ethersWalletToEthereumWallet = ethersWalletToEthereumWallet;
