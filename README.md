# Ethereum adapter library

Ethereum adapter for [Timechain Lib Cryptos](https://bitbucket.org/timechain/tc-lib-cryptos).
## Unit tests

In a terminal, run:

```sh
npm run test
```

To be able to perform transactions, you must set some environment variables.

```bash
# The WIF of the wallet to use to send transactions
export WALLET_PRIVATE_KEY="0xcbf971a1e63ec097d26437b73e24c1cf857bc2bb7d6995d6b3f2ea4db332a87c"

# The recipient address to send transactions
export RECIPIENT_ADDRESS="0x76522d69942B4aEA57AF6146D2fA0DF5a10391fC"

# Set no if you don't want to perform transactions in the tests
export PERFORM_TRANSACTIONS=true

```
