import * as Web3 from 'web3'
import * as ethers from 'ethers'
import { BigNumber } from 'bignumber.js'
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models'
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet'

import { CRYPTO } from './utils/constants'
import { gweiToWei } from './utils/conversion'
import { EthereumWallet } from './models/wallet'
import { getTransactions, toTransaction, getNonce } from './vendor/etherscan'
import { ethersWallet, EthersProvider, ethersBigNumberToBigNumber, toEthersAmount, ethersWalletToEthereumWallet } from './vendor/ethers'

export interface SendEthereumTransactionOptions {
  nonce?: number
}

export class EthereumAdapterClass implements WalletAdapter<EthereumWallet> {
  crypto: string = CRYPTO

  name: string = 'Ethereum'

  decimals = 18

  provider: EthersProvider = new ethers.providers.getDefaultProvider()
  // private provider: EthersProvider = new ethers.providers.JsonRpcProvider()

  async generateRandomWallet (): Promise<{
    wallet: EthereumWallet,
    seed: string
  }> {
    const wallet = new ethers.Wallet.createRandom()

    return {
      seed: wallet.mnemonic,
      wallet: ethersWalletToEthereumWallet(wallet)
    }
  }

  async importWalletWithSeed (seed: string): Promise<EthereumWallet> {
    const wallet = ethers.Wallet.fromMnemonic(seed)
    return ethersWalletToEthereumWallet(wallet)
  }

  async importWallet (info: {
    publicKey?: string;
    privateKey?: string;
    address?: string;
  }): Promise<EthereumWallet> {
    if (!info.privateKey) throw new Error('Private key not provided.')
    const wallet = new ethers.Wallet(info.privateKey)
    return ethersWalletToEthereumWallet(wallet)
  }

  async getBalance (wallet: Partial<Wallet>): Promise<BigNumber> {
    const balance = await this.provider.getBalance(wallet.address!)
    return ethersBigNumberToBigNumber(balance)
  }

  async sendTransaction (options: SendTransactionOptions<EthereumWallet> & SendEthereumTransactionOptions): Promise<Transaction> {
    const ethWallet = ethersWallet(options.wallet.privateKey, this.provider)

    const sendOptions: any = {
      gasLimit: await ethWallet.estimateGas({
        to: options.recipientAddress,
        value: toEthersAmount(options.amount)
      }),
      gasPrice: await this.provider.getGasPrice()
    }

    // Specify nonce, otherwise increment it
    // if (options.nonce) sendOptions.nonce = options.nonce
    // else sendOptions.nonce = (await this.getNonce(options.wallet.address)) + 1

    const transaction = await ethWallet.send(options.recipientAddress, toEthersAmount(options.amount), sendOptions)

    return {
      hash: transaction.hash,
      date: new Date(),
      fromAddress: transaction.from,
      toAddress: transaction.to,
      amount: options.amount,
      crypto: this.crypto
    }
  }

  async getTransactionFees (params: SendTransactionOptions<EthereumWallet>): Promise<BigNumber> {
    const ethWallet = ethersWallet(params.wallet.privateKey, this.provider)

    const gasUsed = await ethWallet.estimateGas({
      to: params.recipientAddress,
      value: toEthersAmount(params.amount)
    })
    const gasPrice = ethersBigNumberToBigNumber(await this.provider.getGasPrice())
    return gasPrice.multipliedBy(gasUsed)
  }

  async getTransactions (wallet: Partial<Wallet>): Promise<Transaction[]> {
    if (!wallet.address) throw new Error('The provided wallet must have an address')

    const transactions: Transaction[] = []
    const etherscanTransactions = await getTransactions(wallet.address)

    for (const transaction of etherscanTransactions) {
      transactions.push(toTransaction(transaction))
    }
    return transactions
  }

  validateAddress (address: string, options: object | null): boolean {
    const web3 = new Web3(this.provider)
    return web3._extend.utils.isAddress(address)
  }

  async getNonce (address: string): Promise<number> {
    return getNonce(address)
  }
}

export const EthereumAdapter = new EthereumAdapterClass()
