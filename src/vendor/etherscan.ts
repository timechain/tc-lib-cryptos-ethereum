import axios, { AxiosResponse } from 'axios'
import { BigNumber } from 'bignumber.js'

import { CRYPTO } from '../utils/constants'
import { Transaction } from 'tc-lib-cryptos/dist/models'

const ETHERSCAN_BASE_URL = 'http://api.etherscan.io/api'
const ETHERSCAN_TOKEN = 'apikey=3HXZA2I1QYB1936W5DYYEC9DUABI49IF64'

const ETHERSCAN_OPTIONS = 'sort=asc'

const GET_BALANCE_ROUTE = 'module=account&action=balance'
const GET_TXLIST_ROUTE = 'module=account&action=txlist'
const GET_NONCE_ROUTE = 'module=proxy&action=eth_getTransactionCount'

interface JSONRPC<T> {
  jsonrpc: string,
  id: string,
  result: T
}

export interface EtherscanTransaction {
  blockNumber: string
  timeStamp: string
  hash: string
  nonce: string
  blockHash: string
  from: string
  contractAddress: string
  to: string
  value: string
  tokenName: string
  tokenSymbol: string
  tokenDecimal: string
  transactionIndex: string
  gas: string
  gasPrice: string
  gasUsed: string
  cumulativeGasUsed: string
  input: string
  confirmations: string
}

export async function getTransactions (address: string): Promise<EtherscanTransaction[]> {
  const url = ETHERSCAN_BASE_URL + '?' + GET_TXLIST_ROUTE + '&address=' + address + '&' + ETHERSCAN_OPTIONS + '&' + ETHERSCAN_TOKEN
  const response = await axios.get<JSONRPC<EtherscanTransaction[]>>(url)
  return response.data.result
}

export function toTransaction (etherscanTransaction: EtherscanTransaction, crypto?: string): Transaction {
  const timestamp = parseInt(etherscanTransaction.timeStamp, 10)
  const transaction: Transaction = {
    hash: etherscanTransaction.hash,
    date: new Date(timestamp * 1000),
    fromAddress: etherscanTransaction.from,
    toAddress: etherscanTransaction.to,
    amount: new BigNumber(etherscanTransaction.value),
    crypto: crypto || CRYPTO
  }
  return transaction
}

export async function getNonce (address: string): Promise<number> {
  const url = `${ETHERSCAN_BASE_URL}?${GET_NONCE_ROUTE}&address=${address}`
  const response = await axios.get<JSONRPC<string>>(url)
  return new BigNumber(response.data.result, 16).toNumber()
}
