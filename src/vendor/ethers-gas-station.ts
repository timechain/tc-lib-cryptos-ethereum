import axios from 'axios'
import { BigNumber } from 'bignumber.js'
import { TransactionFeesPolicy } from 'tc-lib-cryptos/dist/modules/wallet'

export const GAS_API = 'https://ethgasstation.info/json/ethgasAPI.json'

export async function getGasPrice (policy: TransactionFeesPolicy): Promise<BigNumber> {
  const json = (await axios.get(GAS_API)).data
  const gasPrice: number = json.safeLow
  return new BigNumber(gasPrice)
}
