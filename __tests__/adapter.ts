import { BigNumber } from 'bignumber.js'

import { EthereumAdapter } from '../src'
import { CRYPTO } from '../src/utils/constants'
import { Wallet } from 'tc-lib-cryptos/dist/models'
import { EthereumWallet } from '../src/models/wallet'
import { smallestUnitToCryptoUnit } from 'tc-lib-cryptos/dist/modules/wallet'

const TEST_ADDRESS = '0x281055Afc982d96fAB65b3a49cAc8b878184Cb16'

async function environmentWallet (): Promise<EthereumWallet> {
  return EthereumAdapter.importWallet({ privateKey: process.env.WALLET_PRIVATE_KEY! })
}

test('Get generic info', async () => {
  expect(EthereumAdapter.name).toBe('Ethereum')
  expect(EthereumAdapter.crypto).toBe('ETH')
  expect(EthereumAdapter.decimals).toBe(18)
})

test('Generate random wallet', async () => {
  const data = await EthereumAdapter.generateRandomWallet()
  expect(EthereumAdapter.validateAddress(data.wallet.address, null)).toEqual(true)
})

test('Import wallet', async () => {
  function testWallet (wallet: EthereumWallet) {
    expect(EthereumAdapter.validateAddress(wallet.address, null)).toEqual(true)
    expect(wallet.address).toEqual('0xe9AA8a2eAd4A61d602232F8A3D1dDF6CB6a209c9')
  }

  // Seed
  let wallet = await EthereumAdapter.importWalletWithSeed('kingdom rabbit inject install bottom own machine spend result coffee hip off')
  testWallet(wallet)

  // Private key
  wallet = await EthereumAdapter.importWallet({ privateKey: '0x54774c86cfb8474708163deb7613e30ffc37a035a553f6e76fffe994006dc1cb' })
  testWallet(wallet)
})

test('Conversions', () => {
  expect(smallestUnitToCryptoUnit(new BigNumber(123456789), EthereumAdapter).toString(10)).toBe('0.000000000123456789')
})

test('Validate address', () => {
  expect(EthereumAdapter.validateAddress(TEST_ADDRESS, null)).toEqual(true)
  expect(EthereumAdapter.validateAddress('abc', null)).toEqual(false)
  expect(EthereumAdapter.validateAddress('1MPsDH3sUD6xi5YWTbob5WARSKoFreKqbJ', null)).toEqual(false)
})

test('Get balance', async () => {
  const balance = await EthereumAdapter.getBalance({
    address: TEST_ADDRESS
  })
  expect(balance).toBeDefined()
})

test('Get transactions', async () => {
  const transactions = await EthereumAdapter.getTransactions({
    address: TEST_ADDRESS
  })
  expect(transactions.length).toBeGreaterThan(0)
})

test('Estimate transactions fees', async () => {
  const wallet = (await EthereumAdapter.generateRandomWallet()).wallet
  wallet.address = TEST_ADDRESS

  const fees = await EthereumAdapter.getTransactionFees({
    wallet: wallet,
    recipientAddress: TEST_ADDRESS,
    amount: new BigNumber(12345),
    fees: 'minimum'
  })
  expect(fees.toNumber()).toBeGreaterThan(0)
})
